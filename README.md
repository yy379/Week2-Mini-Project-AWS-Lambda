# Week2 Mini Project AWS Lambda

## Description

Create a simple AWS Lambda function that processes data.

## Prerequisites

- Follow the instructions in the [Cargo Lambda](https://www.cargo-lambda.info/guide/getting-started.html) documentation to create a Rust Lambda function.

## Step1: Create IAM Role with permissions

![Create IAM Role](screenshots/permissions.png)

## Step2: Create Lambda Function and add trigger

![Create Lambda Function](screenshots/gateaway.png)

## Step3: Setup API Gateway

![Setup API Gateway](screenshots/method.png)

## Test and Deploy

- Use Json data to test the API Gateway
![Test and Deploy](screenshots/test.png)
- Use the URL to test the API Gateway
![Test and Deploy](screenshots/result.png)